package TwoDimensionalShape;
import Shape.Shape;

public abstract class TwoDimensionalShape extends Shape {
    public abstract double getArea();
}