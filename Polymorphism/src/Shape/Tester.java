package Shape;
import ThreeDimensionalShape.Sphere;
import ThreeDimensionalShape.Cube;
import ThreeDimensionalShape.Tetrahedron;
import TwoDimensionalShape.Circle;
import TwoDimensionalShape.Square;
import TwoDimensionalShape.Triangle;



public class Tester {
    public static void main(String[] args) {

        Shape shape[] = new Shape[6];
        shape[0] = new Circle(5.0);
        shape[1] = new Square(1.9);
        shape[2] = new Triangle(4.0, 5.0);
        shape[3] = new Sphere(7.0);
        shape[4] = new Cube(3.0);
        shape[5] = new Tetrahedron(2.5);

        for (Shape currentShape : shape) {
            System.out.println(currentShape);
            if (currentShape instanceof TwoDimensionalShape.TwoDimensionalShape) {
                TwoDimensionalShape.TwoDimensionalShape twoDimensionalShape = (TwoDimensionalShape.TwoDimensionalShape) currentShape;

                System.out.printf("%sArea: %.2f\n\n", twoDimensionalShape.toString(), twoDimensionalShape.getArea());

            } else if (currentShape instanceof ThreeDimensionalShape.ThreeDimensionalShape) {
                ThreeDimensionalShape.ThreeDimensionalShape threeDimensionalShape = (ThreeDimensionalShape.ThreeDimensionalShape) currentShape;
                System.out.printf("%sArea: %.2f\n\n", threeDimensionalShape.toString(), threeDimensionalShape.getVolume());

                System.out.printf("%sArea: %.2f\n\n", threeDimensionalShape.toString(), threeDimensionalShape.getArea());
            }

        }
    }
}
